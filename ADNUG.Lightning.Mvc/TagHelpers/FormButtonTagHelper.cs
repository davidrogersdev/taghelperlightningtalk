﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace ADNUG.Lightning.Mvc.TagHelpers
{
	[HtmlTargetElement("formbutton")]
	public class FormButtonTagHelper : TagHelper
	{
		public string Type { get; set; }
		public string BgColor { get; set; }

		public override void Process(TagHelperContext context, TagHelperOutput output)
		{
			output.TagName = "button";
			output.TagMode = TagMode.StartTagAndEndTag;
			output.Attributes.SetAttribute("class", $"btn btn-{BgColor}");
			output.Attributes.SetAttribute("type", Type);
			output.Content.SetContent(Type == "submit" ? "Add" : "Reset");
		}
	}
}
