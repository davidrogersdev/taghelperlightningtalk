﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace ADNUG.Lightning.Mvc.TagHelpers
{
	[HtmlTargetElement("form-text-input")]
	public class BootstrapFormInputsHelper : TagHelper
	{
		public const string HelperForName = "ad-for";

		[HtmlAttributeName(HelperForName)]
		public ModelExpression HelperFor { get; set; }

		public override void Process(TagHelperContext context, TagHelperOutput output)
		{
			output.PreElement.SetHtmlContent(@"<div class=""form-group""><label for=" + HelperFor.Name + ">" 
				+ (HelperFor.Metadata.DisplayName ?? HelperFor.Metadata.PropertyName) + 
				"</label>");

			output.PostElement.SetHtmlContent("</div>");

			output.TagName = "input";
			output.TagMode =  TagMode.SelfClosing;
			output.Attributes.SetAttribute("class", "form-control");
			output.Attributes.SetAttribute("value", HelperFor.Model.ToString());
			output.Attributes.SetAttribute("type", "text");
			output.Attributes.SetAttribute("id", HelperFor.Name);
			output.Attributes.SetAttribute("name", HelperFor.Name);
		}
	}
}
