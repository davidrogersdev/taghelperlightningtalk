﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace ADNUG.Lightning.Mvc.TagHelpers
{
	public class BootstrapTableTagHelper : TagHelper
	{
		public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
		{
			var childContent = await output.GetChildContentAsync();
			var content = childContent.GetContent();

			output.TagName = "table";
			output.TagMode = TagMode.StartTagAndEndTag;
			output.Attributes.SetAttribute("class", "table table-hover table-striped");
			output.Content.SetHtmlContent(content);

			await base.ProcessAsync(context, output);
		}
	}
}