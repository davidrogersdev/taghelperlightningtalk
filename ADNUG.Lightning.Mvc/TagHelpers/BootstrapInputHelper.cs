﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace ADNUG.Lightning.Mvc.TagHelpers
{
	[HtmlTargetElement("input", Attributes = "ad-form-input")]
	public class BootstrapInputHelper : TagHelper
	{
		public string AdLabelText { get; set; }
		public string AdNameId { get; set; }

		public override void Process(TagHelperContext context, TagHelperOutput output)
		{
			output.PreElement.SetHtmlContent(@"<div class=""form-group""><label for=" + AdNameId + ">" + AdLabelText + "</label>");
			output.PostElement.SetHtmlContent("</div>");

			output.Attributes.SetAttribute("class", "form-control");
			output.Attributes.SetAttribute("name", AdNameId);
			output.Attributes.SetAttribute("id", AdNameId);
		}
	}

}
