﻿(function () {
    'use strict';

    var crumbSegmentClass = '.crumb-segment',
        linkClass = '.lnk',
        dirList = '#dirList';

    var getDirsFromCrumb = function(e, self) {

        e.preventDefault();
        e.stopPropagation();

        var url = self.getAttribute('href');

        var config = {
            method: 'POST',
            url: url,
            params: { fullPath: self.getAttribute('data-fullpath') },
            headers: { "content-type": "application/json; charset=utf-8" },
            jsonPayload: true
        };

        vanilla.primeAjaxCallPromise(config).then(function(data) {
            var list = document.querySelector(dirList);
            list.innerHTML = data;
        });
    };

    var getDirectoryGrid = function(e, self) {

        e.preventDefault();
        e.stopPropagation();

        if (self.getAttribute('data-type') === 'file') {
            swal('Not a Directory', 'This is a file and cannot drill down.');
        } else {

            var url = self.getAttribute('href');

            var config = {
                method: 'POST',
                url: url,
                params: { fullPath: self.getAttribute('data-fullpath') },
                headers: { "content-type": "application/json; charset=utf-8" },
                jsonPayload: true
            };

            vanilla.primeAjaxCallPromise(config).then(function(data) {
                var list = document.querySelector(dirList);
                list.innerHTML = data;
            });

        }
    };


    document.addEventListener('DOMContentLoaded', function () {

        document.addEventListener('click', function (e) {

            var element = e.target.closest(crumbSegmentClass, this);

            if (element) {
                getDirsFromCrumb(e, element);
            } else {
                element = e.target.closest(linkClass, this);

                if (element)
                    getDirectoryGrid(e, element);
            }
        });

    });

}());








