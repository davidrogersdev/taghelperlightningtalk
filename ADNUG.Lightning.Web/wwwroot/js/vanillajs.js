﻿/*global document:false, Element:false */
var vanilla = (function () {
    'use strict';

    if (Element && !Element.prototype.closest) {
        Element.prototype.closest = function (selector, filter) {

            var node = this, found;

            filter = typeof filter === 'string' ? document.querySelector(filter) : filter;

            while (node instanceof Element && !(found = node.matches(selector)) && node !== filter) {
                node = node.parentNode;
            }

            return found ? node : null;
        };
    }

    var primeAjaxCall = function(config) {

        var xhr = new XMLHttpRequest();
        xhr.open('POST', config.url, true);
        xhr.setRequestHeader('content-type', 'application/json; charset=utf-8');

        return xhr;
    };

    var primeAjaxCallPromise = function(config) {

        return new Promise(function(resolve, reject) {

            var xhr = new XMLHttpRequest();
            xhr.open(config.method, config.url, true);

            xhr.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    resolve(xhr.response);
                } else {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                }
            };
            xhr.onerror = function () {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            };

            if (config.headers) {
                Object.keys(config.headers).forEach(function (key) {
                    xhr.setRequestHeader(key, config.headers[key]);
                });
            }

            var params = config.params;

            // We'll need to stringify if we've been given an object
            // If we have a string, this is skipped.
            if (params && typeof params === 'object') {

                if (config.jsonPayload && config.jsonPayload === true) {
                    params = JSON.stringify(params);
                } else {
                    params = Object.keys(params).map(function (key) {
                        return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
                    }).join('&');
                }
            }

            xhr.send(params);
        });


    };

    return {
        primeAjaxCall: primeAjaxCall,
        primeAjaxCallPromise: primeAjaxCallPromise
    };
}());
