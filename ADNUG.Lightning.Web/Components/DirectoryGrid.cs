﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ADNUG.Lightning.Hardware.Model;
using ADNUG.Lightning.Hardware.Services;
using ADNUG.Lightning.Web.Infrastructure.Extensions;
using ADNUG.Lightning.Web.Models.DataTransferObjects;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ADNUG.Lightning.Web.Components
{
	public class DirectoryGrid : ViewComponent
    {
        private readonly IHardDriveInfoService _hardDriveInfoService;

        public DirectoryGrid(IHardDriveInfoService hardDriveInfoService)
        {
            _hardDriveInfoService = hardDriveInfoService;
        }

        public async Task<IViewComponentResult> InvokeAsync(string fullPath)
        {
            var model = await _hardDriveInfoService.GetDirectoryContentsAsync(fullPath);

			// if NullOrEmpty, we know an Unauth Exception has been chucked.
			if (string.IsNullOrEmpty(model.FullPath))
		        return View(new DriveInfoDto
		        {
			        BreadCrumbTrail = new HtmlString(string.Empty),
					FolderGrid = Enumerable.Empty<IEnumerable<DriveFolderDto>>()
		        });

            var driveInfoDto = GetFolderContents(model);

            driveInfoDto.BreadCrumbTrail = ConstructBreadCrumb(fullPath);

            return View(driveInfoDto);
        }

        private static HtmlString ConstructBreadCrumb(string fullPath)
        {
            var segments =
                fullPath.Split(Path.DirectorySeparatorChar.ToString().ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                    .ToArray();

            var segmentsForPath = segments.ToArray();

            var pathSegments = segments
                .Select((item, idx) =>
                {
                    var uri = segmentsForPath
                        .Take(idx + 1)
                        .Aggregate((agg, next) =>
                        {
                            if (agg.EndsWith(":"))
                                agg = string.Concat(agg, Path.DirectorySeparatorChar);

                            return Path.Combine(agg, next);
                        });

                    var linkBuilder = new TagBuilder("a") { TagRenderMode = TagRenderMode.Normal};
                    linkBuilder.MergeAttribute("href", "/api/HardDrive/GetContents");
                    linkBuilder.MergeAttribute("data-fullpath", uri);
                    linkBuilder.AddCssClass("crumb-segment");
                    linkBuilder.InnerHtml.AppendHtml(item);

                    return linkBuilder.ProcessTagBuilderAsString();

                }).ToArray();


            var breadCrumbRaw = string.Join(" > ", pathSegments);

            return new HtmlString(breadCrumbRaw);
        }

        private DriveInfoDto GetFolderContents(DriveFolder model)
        {
            // kinda long method, but I wanted to keep the whole algorithm together (ಠ‿ಠ)

            int columnCount = 5;
            int count = 0;
            int idx = 0;
            int currentRowIndex;

            var numDirectories = model.DriveFolders.Count;
            var numFiles = model.Files.Count;
            var totalArtifacts = numDirectories + numFiles;
            var rem = totalArtifacts % columnCount > 0 ? 1 : 0;

            // if it is not a clean division, add 1 row to pick up the remainder.
            var numRows = totalArtifacts / columnCount + rem;

            var folders = new List<List<DriveFolderDto>>(
                    Enumerable.Range(0, numRows).Select(i => new List<DriveFolderDto>(
                        Enumerable.Range(0, columnCount).Select(j => new DriveFolderDto())
                    )));

            for (int index = 0; index < columnCount * numRows; index++)
            {
                var folder = index < numDirectories
                    ? model.DriveFolders[index]
                    : null;

                if(folder == null)
                    break;

                if (idx == columnCount)
                {
                    count++;
                    idx = 0;
                }

                folders[count][idx] = new DriveFolderDto
                {
                    Name = new HtmlString(folder.Name),
                    FullPath = folder.FullPath,
                    Icon = "fa-folder"
                };
                idx++;
            }

            currentRowIndex = count;
            var nonBreakingSpace = "&nbsp;";

            for (int index = 0; index <  (numRows - currentRowIndex) * columnCount; index++)
            {
                var file = index < numFiles
                    ? model.Files[index]
                    : new ADNUG.Lightning.Hardware.Model.File { Name = nonBreakingSpace };

                if (idx == columnCount)
                {
                    if (count < numRows - 1)
                    {
                        count++;
                        idx = 0;
                    }
                    else
                    {
                        break;
                    }
                }

                folders[count][idx] = new DriveFolderDto
                {
                    Name = new HtmlString(file.Name),
                    Empty = file.Name == nonBreakingSpace,
                    FullPath = file.FullPath,
                    Icon = "fa-file-o" 
                };
                idx++;
            }

            return new DriveInfoDto
            {
                FolderGrid = folders
            };
        }
    }
}
