﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ADNUG.Lightning.Web.Infrastructure.Extensions
{
	public static class SelectListHelperExtensions
    {
	    private const string Key = "Key";
	    private const string Value = "Value";

	    public static SelectList ToSelectList<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> list, object selectedValue = null)
        {
            return new SelectList(list ?? new List<KeyValuePair<TKey, TValue>>(), Key, Value, selectedValue);
        }

        public static SelectList ToSelectList<T, T2>(this IDictionary<T, T2> dictionary, object selectedValue = null)
        {
            return new SelectList(dictionary ?? new Dictionary<T, T2>(), Key, Value, selectedValue);
        }
    }
}
