﻿using Microsoft.AspNetCore.Html;

namespace ADNUG.Lightning.Web.Models.DataTransferObjects
{
    public class DriveFolderDto
    {
        public IHtmlContent Name { get; set; }
        public string FullPath { get; set; }
        public string Icon { get; set; }
        public bool Empty { get; set; }
    }
}
