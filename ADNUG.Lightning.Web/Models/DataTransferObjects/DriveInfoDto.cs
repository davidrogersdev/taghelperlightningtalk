﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Html;

namespace ADNUG.Lightning.Web.Models.DataTransferObjects
{
	public class DriveInfoDto
    {
        public IHtmlContent BreadCrumbTrail { get; set; }
        public IEnumerable<IEnumerable<DriveFolderDto>> FolderGrid { get; set; }
    }
}
