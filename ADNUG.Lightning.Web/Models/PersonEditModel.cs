﻿using System.ComponentModel;

namespace ADNUG.Lightning.Web.Models
{
    public class PersonEditModel
    {
		[DisplayName("First Name")]
	    public string FirstName { get; set; }
	    [DisplayName("Last Name")]
		public string LastName { get; set; }
	    public int Age { get; set; }
    }
}
