﻿using System.Collections.Generic;
using ADNUG.Lightning.Web.Models.DataTransferObjects;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ADNUG.Lightning.Web.Models
{
	public class HardDriveViewModel
    {
        public IList<List<DriveFolderDto>> DriveFolders { get; set; }
        public IEnumerable<SelectListItem> DriveLetters { get; set; }

        public string SelectedDriveId { get; set; }
    }
}
