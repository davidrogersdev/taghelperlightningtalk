using System.Threading.Tasks;
using ADNUG.Lightning.Hardware.Model;
using ADNUG.Lightning.Hardware.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ADNUG.Lightning.Web.Controllers.api
{
    [Route("api/[controller]")]
    public class HardDriveController : Controller
    {
        private readonly IHardDriveInfoService _hardDriveInfoService;

        public HardDriveController(IHardDriveInfoService hardDriveInfoService, ILoggerFactory loggerFactory)
        {
            _hardDriveInfoService = hardDriveInfoService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _hardDriveInfoService.GetDirectoryContentsAsync(@"D:\"));
        }


	    [HttpPost]
	    [Route("[action]")]
	    public IActionResult GetContents([FromBody] DriveFolder model)
	    {
		    return ViewComponent("DirectoryGrid", new {fullPath = model.FullPath});
	    }
    }
}