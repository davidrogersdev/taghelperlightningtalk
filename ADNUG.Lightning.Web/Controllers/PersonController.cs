﻿using ADNUG.Lightning.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace ADNUG.Lightning.Web.Controllers
{
    public class PersonController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
	        var person = new PersonEditModel
	        {
		        Age = 54,
		        FirstName = "Dutch",
		        LastName = "Dixon",
	        };

            return View(person);
        }

	    public IActionResult BuiltInTagHelpers()
	    {
		    var person = new PersonEditModel
		    {
			    Age = 54,
			    FirstName = "Dutch",
			    LastName = "Dixon",
		    };

		    return View(person);
	    }
	}
}
