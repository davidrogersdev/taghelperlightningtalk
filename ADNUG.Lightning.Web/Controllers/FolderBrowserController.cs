﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ADNUG.Lightning.Hardware.Services;
using ADNUG.Lightning.Web.Infrastructure.Extensions;
using ADNUG.Lightning.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace ADNUG.Lightning.Web.Controllers
{
	public class FolderBrowserController : Controller
    {
	    private readonly IHardDriveInfoService _hardDriveInfoService;

	    public FolderBrowserController(IHardDriveInfoService hardDriveInfoService)
	    {
		    _hardDriveInfoService = hardDriveInfoService;
	    }


		public async Task<IActionResult> Index()
        {
	        var hardDrives = await _hardDriveInfoService.GetHardDrivesAsync();

	        var hardDriveViewModel = new HardDriveViewModel
	        {
		        DriveLetters = hardDrives
			        .Select((d, i) =>
			        {
				        var driveLetter = d.Name.Substring(0, 1);
				        return new KeyValuePair<string, string>(driveLetter, driveLetter);
			        })
			        .ToSelectList(),
		        SelectedDriveId = @"C:\"
	        };

	        return View(hardDriveViewModel);
		}


	    [HttpGet]
	    public async Task<IActionResult> IndexWithId(string selectedDriveId)
	    {
		    var hardDrives = await _hardDriveInfoService.GetHardDrivesAsync();

		    var hardDriveViewModel = new HardDriveViewModel
		    {
			    DriveLetters = hardDrives
				    .Select((d, i) =>
				    {
					    var driveLetter = d.Name.Substring(0, 1);
					    return new KeyValuePair<string, string>(driveLetter, driveLetter);
				    })
				    .ToSelectList(),
			    SelectedDriveId = selectedDriveId + @":\"
		    };

		    return View("Index", hardDriveViewModel);
	    }

	}
}
