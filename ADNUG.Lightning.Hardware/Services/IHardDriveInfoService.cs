﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ADNUG.Lightning.Hardware.Model;

namespace ADNUG.Lightning.Hardware.Services
{
    public interface IHardDriveInfoService
    {
        DriveFolder GetDirectories(string drive);
        Task<DriveFolder> GetDirectoryContentsAsync(string folderPath);
        Task<IEnumerable<DriveInfo>> GetHardDrivesAsync();
    }
}