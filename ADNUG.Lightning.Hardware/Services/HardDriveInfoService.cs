using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ADNUG.Lightning.Hardware.Model;
using File = ADNUG.Lightning.Hardware.Model.File;

namespace ADNUG.Lightning.Hardware.Services
{
    public class HardDriveInfoService : IHardDriveInfoService
    {
        public HardDriveInfoService()
        {

        }

        public async Task<IEnumerable<DriveInfo>> GetHardDrivesAsync()
        {
            var allDrives = await Task.Run(() =>
            {
                return DriveInfo.GetDrives()
                        .Where(d => d.DriveType != DriveType.CDRom 
                            && (d.DriveType == DriveType.Removable || d.DriveType == DriveType.Fixed));
            });

            return allDrives;
        }

        public async Task<DriveFolder> GetDirectoryContentsAsync(string folderPath)
        {
            if (folderPath.EndsWith(":"))
                folderPath = folderPath + @"\\";

            var directoryInfo = new DirectoryInfo(folderPath);

            var result = await Task.Run(() =>
            {
	            try
	            {
		            return new DriveFolder
		            {
			            FullPath = folderPath,
			            DriveFolders = directoryInfo.GetDirectories()
				            .Where(d => (d.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
				            .Select(d => new DriveFolder
				            {
					            Name = d.Name,
					            Files = d.GetFiles()
						            .Where(f => (f.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
						            .Select(f => new File
						            {
							            Name = f.Name
						            })
						            .ToList(),
					            FullPath = d.FullName,
				            })
				            .ToList(),
			            Files = directoryInfo.GetFiles()
				            .Where(f => (f.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
				            .Select(f => new File
				            {
					            FullPath = f.FullName,
					            Name = f.Name
				            })
				            .ToList(),
			            Name = directoryInfo.Name
		            };
	            }
	            catch (UnauthorizedAccessException unauthorizedAccessException)
	            {
		            return new DriveFolder{ FullPath = string.Empty, Name = string.Empty, Files = Enumerable.Empty<File>().ToList()};
	            }
				catch (Exception)
	            {
		            throw;
	            }
            }); 

            return result;
        }

        public DriveFolder GetDirectories(string drive)
        {
            DirectoryInfo rootDrive = new DirectoryInfo(drive);

            var driveFolderRoot = new DriveFolder
            {
                DriveFolders = new List<DriveFolder>(),
                Files = new List<File>(),
                Name = drive
            };

            GetDirectoriesHelper(rootDrive, driveFolderRoot);

            return driveFolderRoot;
        }

        private void GetDirectoriesHelper(DirectoryInfo folder, DriveFolder driveFolder)
        {
            var nonHiddenFolders =
                folder.GetDirectories().Where(d => (d.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden);

            if (!nonHiddenFolders.Any())
            {
                driveFolder.Files = folder.GetFiles()
                    .Where(d => (d.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                    .Select(f => new File { Name = f.Name })
                    .ToList();
            }

            foreach (var directoryInfo in nonHiddenFolders)
            {
                var driveFolderOfFolder = new DriveFolder
                {
                    DriveFolders = new List<DriveFolder>(),
                    Files = new List<File>(),
                    Name = directoryInfo.Name
                };

                GetDirectoriesHelper(directoryInfo, driveFolderOfFolder);
                driveFolder.DriveFolders.Add(driveFolderOfFolder);
            }
        }
    }
}