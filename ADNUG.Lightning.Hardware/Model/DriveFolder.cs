using System.Collections.Generic;
using System.Diagnostics;

namespace ADNUG.Lightning.Hardware.Model
{
    [DebuggerDisplay("Name = {Name}, FullPath = {FullPath}")]
    public class DriveFolder
    {
        public IList<DriveFolder> DriveFolders { get; set; }
        public IList<File> Files { get; set; }
        public string Name { get; set; }
        public string FullPath { get; set; }
    }
}