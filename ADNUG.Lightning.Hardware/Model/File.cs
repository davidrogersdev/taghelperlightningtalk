using System.Diagnostics;

namespace ADNUG.Lightning.Hardware.Model
{
    [DebuggerDisplay("Name = {Name}, FullPath = {FullPath}")]
    public class File
    {
        public string Name { get; set; }
        public string FullPath { get; set; }
    }
}
