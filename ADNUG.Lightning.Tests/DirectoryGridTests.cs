﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ADNUG.Lightning.Hardware.Model;
using ADNUG.Lightning.Hardware.Services;
using ADNUG.Lightning.Web.Components;
using ADNUG.Lightning.Web.Infrastructure.Extensions;
using ADNUG.Lightning.Web.Models.DataTransferObjects;
using FakeItEasy;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ADNUG.Lightning.Web.Tests
{
    [TestClass]
    public class DirectoryGridTests
    {
        private string FolderName = @"D:\";
        private string SubFolderName = @"D:\Amd";

        [TestMethod]
        public async Task DirectoryGrid_Returns_Breadcrumb_Directory()
        {
            var fake = A.Fake<IHardDriveInfoService>();

            A.CallTo(() => fake.GetDirectoryContentsAsync(FolderName))
                .Returns(Task.FromResult(new DriveFolder
                {
                    DriveFolders = new List<DriveFolder>
                    {
                        new DriveFolder
                        {
                            DriveFolders = new List<DriveFolder>(0),
                            Files = new List<File>(0),
                            FullPath = SubFolderName,
                            Name = SubFolderName
                        }
                    },
                    Files = new List<File>(0),
                    FullPath = FolderName,
                    Name = FolderName,
                }));

            var viewComponent = new DirectoryGrid(fake);

            var result = await viewComponent.InvokeAsync(FolderName);

            Assert.AreEqual(typeof(DriveInfoDto), ((ViewViewComponentResult)result).ViewData.Model.GetType());
            Assert.AreEqual(GetExpectedBreadCrumb().ProcessIHtmlContentAsString(), ((DriveInfoDto)((ViewViewComponentResult)result).ViewData.Model).BreadCrumbTrail.ProcessIHtmlContentAsString());
        }

        public HtmlString GetExpectedBreadCrumb()
        {
            var linkBuilder = new TagBuilder("a") { TagRenderMode = TagRenderMode.Normal };
            linkBuilder.MergeAttribute("href", "/api/HardDrive/GetContents");
            linkBuilder.MergeAttribute("data-fullpath", "D:");
            linkBuilder.AddCssClass("crumb-segment");
            linkBuilder.InnerHtml.AppendHtml("D:");

            return new HtmlString(linkBuilder.ProcessTagBuilderAsString());
        }

    }
}
